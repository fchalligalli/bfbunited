#include <Rcpp.h>
#include "helpers.h"

std::vector<double> calcPlaygoaldist(
    std::pair<unsigned char, unsigned char> playgoaldist_key
){
  unsigned char attempts = playgoaldist_key.first;
  double efficiency = playgoaldist_key.second / (14.0 * 15.0);
  double q = 1.0 - efficiency;
  double nfac = fac(attempts);
  std::vector<double> playgoaldist(attempts + 1);
  for (unsigned char k = 0; k <= attempts; k++) {
    playgoaldist[k] = nfac / (fac(k) * fac(attempts - k)) *
      pow(efficiency, k) * pow(q, attempts - k);
  }
  return playgoaldist;
}
