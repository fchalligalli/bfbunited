#' Einlesen von ods- oder csv-Dateien
#'
#' Für ods-Dateien wird [readODS::read_ods()] aus verwendet. Bei csv-Dateien
#' wird [data.table::fread()] genutzt und als Trennzeichen der Tabulator
#' (`"\t"`) erwartet. Für ein zuverlässiges Ergebnis sollte die Datei nicht mit
#' leeren Zeilen beginnen oder unvollständige Zeilen aufweisen.
#'
#' @author Philipp Hallmeier
#' @param file `character(1)`.
#' @return `data.table` der Inputdatei mit Großbuchstaben als Spaltennamen und
#'   `character` als Spaltentyp. Leere Spalten und Zeilen werden nicht
#'   übersprungen und als `""` dargestellt.
#' @seealso Umkehrfunktion [writeFile()].
#' @export
readFile <- function(file){
  assertString(file)

  file.extension <- tail(strsplit(file, split = ".", fixed = TRUE)[[1]], 1L)

  if (file.extension == "ods") {
    dt <- setDT(readODS::read_ods(
      file, sheet = 1, col_names = FALSE, col_types = NA, na = character(),
      as_tibble = FALSE, .name_repair = "minimal", ods_format = "ods",
      trim_ws = FALSE
    ))
  } else if (file.extension == "csv") {
    dt <- fread(file = file, header = FALSE, sep = "\t", na.strings = NULL,
                colClasses = "character")
  } else {
    stop("Dateiendung muss ods oder csv sein")
  }
  setnames(dt, LETTERS[seq_len(ncol(dt))])
  return(dt[])
}

#' Schreiben von `data.table`s in csv-Dateien
#'
#' Als Spaltentyp ist ausschließlich `character` erlaubt. Als Trennzeichen wird
#' der Tabulator (`"\t"`) verwendet. Spaltennamen werden nicht geschrieben.
#' Umkehrfunktion von [readFile()].
#'
#' @author Philipp Hallmeier
#' @param dt \code{data.table}.
#' @param file \code{character(1)}.
#' @return \code{invisible(NULL)}.
#' @export
writeFile <- function(dt, file){
  assertDataTable(dt, types = "character", any.missing = FALSE)
  assertString(file)

  fwrite(dt, file = file, sep = "\t", col.names = FALSE, quote = FALSE)
}

#' Schreiben von `data.table`s mit gleicher Breite innerhalb von Spalten
#'
#' Wrapper um [writeFile()], der Strings einer Spalte Leerzeichen anhängt, um
#' ihnen die gleiche Breite zu geben. Dadurch sollen die Dateien in Texteditoren
#' besser lesbar sein.
#'
#' @author Philipp Hallmeier
#' @inherit writeFile params return
#' @export
writeTxtFile <- function(dt, file){
  assertDataTable(dt, types = "character")

  dt <- dt[, lapply(.SD, format)]

  writeFile(dt, file)
}
