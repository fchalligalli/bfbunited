#' Spieler ausgeben
#'
#' Übersetzt die Programmdarstellung eines Spielers in seine textuelle
#' Darstellung.
#'
#' @author Philipp Hallmeier
#' @template unitedIn
#' @param player einzeilige `data.table`. Spalten wie im Rückgabewert von
#'               [readPlayer()].
#' @param competition.name `character(1)`. Name des Wettbewerbs, für den die
#'   Tore gezählt werden sollen. Ohne Angabe werden die Tore nicht gezählt.
#' @param team.name `character(1)`. Name des Teams des Spielers (für die
#'   Torzählung).
#' @return Einzeilige `data.table`.
formatPlayer <- function(united, player, competition.name = "", team.name = ""){
  assertUnited(united)
  assertDataTable(player, nrows = 1)

  pos <- paste0(if (player$Is.T) "T",
                if (player$Is.A) "A",
                if (player$Is.V) "V",
                if (player$Is.M) "M",
                if (player$Is.S) "S",
                if (!any(player$Is.T, player$Is.A, player$Is.V, player$Is.M,
                         player$Is.S)) "F")
  age <- names(age.names)[age.names == player$Age]

  # Tore zählen
  if (competition.name != "") {
    competitions <- unlist(lapply(united$schedule, `[[`, "competitions"), FALSE)
    games <- unlist(lapply(unlist(lapply(competitions[vapply(
      competitions, `[[`, "", "name") == competition.name], `[[`, "matchdays"),
      FALSE), `[[`, "games"), FALSE)
    goals <- 0L
    for (i in seq_along(games)) {
      if (is.null(games[[i]]$result)) next
      if (games[[i]]$info$Home == team.name) {
        scorers <- games[[i]]$result$goals[Side == "home" & Type != "owngoal",
                                           Scorer]
      } else if (games[[i]]$info$Away == team.name) {
        scorers <- games[[i]]$result$goals[Side == "away" & Type != "owngoal",
                                           Scorer]
      } else {
        next
      }
      goals <- goals + sum(scorers == player$Name)
    }
    goals <- paste0(goals, " Tor", if (goals != 1) "e")
  } else {
    goals <- ""
  }

  return(data.table(A = player$Name, B = paste(pos, age, player$Level),
                    C = paste(c(if ((player$WP != 1 && !player$Is.T &&
                                     !player$Is.A) ||
                                    (player$WP != 2 && (player$Is.T ||
                                                        player$Is.A))) {
                      paste0("[", formatC(player$WP, digits = 1, format = "f",
                                          dec = ","), "]")
                    }, if (player$Trainable != 3) {
                      paste0("/", player$Trainable)
                    }, if (!is.na(player$Level.cap)) {
                      paste0(">", player$Level.cap)
                    }, if (player$Aging.bonus != 0) {
                      paste0("^", player$Aging.bonus)
                    }, if (player$Banned > 0) {
                      paste0("(", player$Banned, ")")
                    }, if (player$Locked > 0) {
                      paste0("#", player$Locked)
                    }, if (player$Downsell.locked) {
                      "§"
                    }, if (player$Exp.V > 0) {
                      paste0(player$Exp.V, "*V")
                    }, if (player$Exp.M > 0) {
                      paste0(player$Exp.M, "*M")
                    }, if (player$Exp.S > 0) {
                      paste0(player$Exp.S, "*S")
                    }, if (player$Practice > 0) {
                      paste0(player$Practice, "*eingespielt")
                    }), collapse = " "), D = paste(player$DP, "DP"),
                    E = goals, F = paste("trainierbar bis:", player$Season.cap),
                    G = paste("NL-Wert:", calcNLValue(player),
                              attr(united, "currency"))))
}

#' WP der stärksten Aufstellung
#'
#' Berechnet die maximalen WP, die ein Team aufs Feld bringen kann. Sperren und
#' die 3:1-Regel werden nicht berücksichtigt.
#'
#' @author Philipp Hallmeier
#' @param players `data.table` der Spieler. Spalten wie im Rückgabewert von
#'                [readPlayer()].
#' @return `integer(1)`.
WPin11 <- function(players){
  assertDataTable(players)

  players <- players[Level > 0 & Age >= 0, ]
  T <- max(0L, players[(Is.T), Level]) * 2L
  players <- players[!(Is.T), ]
  field <- players[!(Is.A), Level - !(Is.V | Is.M | Is.S)]
  As <- players[(Is.A), ]
  if (nrow(As) > 0) {
    As <- As[order(-Level, Is.V | Is.M | Is.S), ]
    A <- As[1, Level] * 2L
    As <- As[-1, ]
    # Übrige Ausputzer als Feldspieler zulassen:
    field <- c(field, As[, Level - !(Is.V | Is.M | Is.S)])
  } else {
    A <- 0L
  }
  if (length(field) > 9) {
    field <- sort(field, decreasing = TRUE)
    if (field[10] > A) A <- field[10]  # Spiel ohne Ausputzer
    field <- field[1:9]
  }
  return(sum(T, A, field))
}

#' Team ausgeben
#'
#' Übersetzt die Programmdarstellung eines Teams in seine textuelle Darstellung.
#'
#' @author Philipp Hallmeier
#' @template unitedIn
#' @param team Liste mit Elementen wie im Rückgabewert von [readTeam()].
#' @return `data.table`.
#' @seealso [WPin11()], [formatPlayer()]
formatTeam <- function(united, team){
  assertUnited(united)
  assertList(team)

  players <- copy(team$players)
  n <- nrow(players)
  fteam <- as.data.table(matrix("", 2, 7))
  setnames(fteam, LETTERS[1:7])
  fteam[1, A := team$name]
  fteam[1, B := paste(team$GE, attr(united, "currency"))]
  fteam[1, C := paste(formatC(team$WP, digits = 1, format = "f", dec = ","),
                      "WP")]
  fteam[2, D := paste(team$DP, "DP")]
  fteam[1, D := paste(team$MP, "MP")]
  fteam[1, E := team$competition]
  fteam[1, F := team$short.name]
  fteam[1, G := team$supershort.name]

  fteam[2, A := paste(n, "Spieler")]
  WP.11 <- WPin11(players[Banned == 0, ])
  fteam[2, B := paste("WP in 11:", WP.11)]
  if (players[, any(Banned > 0)]) {
    WP.11.banned <- WPin11(players)
    if (WP.11.banned > WP.11) {
      fteam[2, B := paste0(B, " (", WP.11.banned, ")")]
    }
  }
  fteam[2, F := paste("trainierbare Spieler:",
                      players[, sum(Level < Season.cap)])]

  fteam <- rbindlist(c(list(fteam),
                       lapply(seq_len(nrow(players)), function(i){
                         formatPlayer(united, players[i, ],
                                      competition.name = team$competition,
                                      team.name = team$name)
                       })))
  if (team$competition != "") {
    goals <- sum(as.integer(tstrsplit(
      fteam[seq.int(3, length.out = n), E], " ", fixed = TRUE, keep = 1)[[1]]))
    fteam[2, E := paste0(goals, " Tor", if (goals != 1) "e")]
  }
  fteam[2, G := paste("NL-Wert:", sum(as.integer(tstrsplit(
    fteam[seq.int(3, length.out = n), G], " ", fixed = TRUE, keep = 2)[[1]])),
    attr(united, "currency"))]
  return(fteam)
}

#' Vereine in eine csv-Datei schreiben
#'
#' Schreibt United-Teams in eine csv-Datei, wo diese dann zum Wiedereinlesen mit
#' [setTeams()] bearbeitet werden können.
#'
#' @author Philipp Hallmeier
#' @template unitedIn
#' @inherit writeFile params return
#' @seealso [writeTeamFiles()], [formatTeam()], [formatPlayer()]
#' @export
writeTeams <- function(united, file){
  assertUnited(united)
  assertString(file)

  n <- length(united$teams) * 2L
  fteams <- vector("list", n)
  fteams[[1]] <- data.table(A = character(), B = character(), C = character(),
                            D = character(), E = character(), F = character(),
                            G = character())
  even <- rep_len(c(FALSE, TRUE), n)
  fteams[even] <- lapply(united$teams, formatTeam, united = united)
  fteams[which(!even)[-1]] <- list(data.table(A = "", B = "", C = "", D = "",
                                              E = "", F = "", G = ""))
  fteams <- rbindlist(fteams)
  writeFile(fteams, file)
}

#' Vereine in separate csv-Dateien schreiben
#'
#' Schreibt jedes United-Team in eine eigene csv-Datei, die den Teamnamen trägt.
#'
#' @author Philipp Hallmeier
#' @inherit writeTeams params return
#' @param dir `character(1)`. Zielordner. Standardmäßig das aktuelle working
#'            directory
#' @param mode `character(1)`. Welche Arten von Dateien erzeugt werden sollen.
#'             Kann abgekürzt werden.
#' @seealso [writeTeams()], [formatTeam()], [formatPlayer()]
#' @export
writeTeamFiles <- function(united, dir = ".", mode = c("csv", "txt")){
  assertUnited(united)
  assertString(dir)
  mode <- match.arg(mode)

  if (!dir.exists(dir)) dir.create(dir)
  fteams <- lapply(united$teams, formatTeam, united = united)
  filenames <-
    file.path(dir, paste0(vapply(united$teams, `[[`, "", "name"), ".", mode))
  mapply(if (mode == "csv") writeFile else writeTxtFile, fteams, filenames,
         SIMPLIFY = FALSE, USE.NAMES = FALSE)
  return(invisible(NULL))
}
