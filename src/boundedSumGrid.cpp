#include <RcppArmadillo.h>
using namespace Rcpp;
using namespace arma;

// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::export(rng=false, name="C_boundedSumGrid")]]
IntegerMatrix boundedSumGrid(List args, int lb, int ub, IntegerVector w) {
  unsigned nargs = args.size();

  imat crange(nargs, 2);
  for (unsigned i = 0; i < nargs; i++) {
    IntegerVector arg = args[i];
    crange(i, 0) = min(arg) * w[i];
    crange(i, 1) = max(arg) * w[i];
  }

  imat grid(1, 0);
  for (unsigned i = 0; i < nargs; i++) {
    IntegerVector wp(0);
    if (i > 0) wp = w[Range(0, i - 1)];
    IntegerVector row_sums = wrap(grid * as<ivec>(wp));
    unsigned n = row_sums.size();
    IntegerVector lower_bound = lb - row_sums;
    IntegerVector upper_bound = ub - row_sums;
    if (i != nargs - 1) {
      lower_bound = lower_bound -
        sum(as<IntegerVector>(wrap(crange.submat(i + 1, 1, nargs - 1, 1))));
      upper_bound = upper_bound -
        sum(as<IntegerVector>(wrap(crange.submat(i + 1, 0, nargs - 1, 0))));
    }
    IntegerVector arg = args[i];
    IntegerVector warg = arg * w[i];

    List expansion(n);
    unsigned exp_grid_size = 0;
    for (unsigned j = 0; j < n; j++) {
      IntegerVector expansion_j = arg[(warg >= lower_bound[j]) &
        (warg <= upper_bound[j])];
      expansion[j] = expansion_j;
      exp_grid_size += expansion_j.size();
    }

    imat exp_grid(exp_grid_size, i + 1);
    uword r = 0;
    for (unsigned j = 0; j < n; j++) {
      IntegerVector expansion_j = expansion[j];
      for (IntegerVector::iterator expansion_j_k = expansion_j.begin();
           expansion_j_k != expansion_j.end(); ++expansion_j_k) {
        if (i > 0) exp_grid.submat(r, 0, r, i - 1) = grid.row(j);
        exp_grid(r, i) = *expansion_j_k;
        r++;
      }
    }
    grid = exp_grid;
  }
  return wrap(grid);
}
