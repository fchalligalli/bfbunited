#include <Rcpp.h>
#include "helpers.h"
#include "boundedSumGrid.h"
#include <boost/container_hash/hash.hpp>
using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::depends(BH)]]
// [[Rcpp::export(rng=false, name="C_listFormations")]]
List listFormations(std::vector<unsigned char> base, DataFrame carddraw,
                    unsigned char effort, NumericMatrix card_matrix) {

  // alle möglichen Rotkombinationen der Spieler als logischer Vektor
  unsigned char n = carddraw.nrow();
  List list01 = rep(List::create(IntegerVector::create(0, 1)), n);
  IntegerVector w(n, 1);
  LogicalMatrix redgrid;
  unsigned char redgrid_redcards = 0;
  NumericVector playerprob = carddraw["Prob"];
  std::unordered_map<std::vector<bool>, double> reddist;
  for (unsigned char cards = 0; cards < card_matrix.nrow(); cards++) {
    double p = card_matrix(cards, effort);
    if (p == 0) continue;
    if (cards < 2) {
      reddist[std::vector<bool>(n)] += p;
      continue;
    }
    unsigned char redcards = cards / 2;
    if (redgrid_redcards != redcards) {
      redgrid = as<LogicalMatrix>(boundedSumGrid(list01, 0, redcards, w));
      redgrid_redcards = redcards;
    }
    for (unsigned i = 0; i < redgrid.nrow(); i++) {
      LogicalVector reds = redgrid.row(i);
      List cardlist(n);
      for (unsigned char j = 0; j < n; j++) {
        if (reds[j]) {
          cardlist[j] = Range(2, cards);
        } else {
          cardlist[j] = IntegerVector::create(0, 1);
        }
      }
      IntegerMatrix cardgrid = boundedSumGrid(cardlist, cards, cards, w);
      double pp = 0.0;
      unsigned fcards = ifac(cards);
      for (unsigned k = 0; k < cardgrid.nrow(); k++) {
        pp += fcards / prod(factorial(cardgrid.row(k))) *
          powprod(playerprob, cardgrid.row(k));  // dmultinom
      }
      reddist[as< std::vector<bool> >(reds)] += p * pp;
    }
  }

  // Überführung in Rotstärken der Positionen
  std::unordered_map< std::array<std::multiset<unsigned char>, 5>, double,
                      boost::hash< std::array<std::multiset<unsigned char>,
                                              5> > > position_reddist;
  IntegerVector home_position = carddraw["Position"];
  home_position = home_position - 1;
  IntegerVector home_level = carddraw["Level"];
  for (std::unordered_map<std::vector<bool>, double>::iterator it =
       reddist.begin(); it != reddist.end(); ++it) {
    std::vector<bool> reds = (*it).first;
    std::array<std::multiset<unsigned char>, 5> position_reds;
    for (unsigned char i = 0; i < n; i++) {
      if (reds[i]) {
        position_reds[home_position[i]].insert(home_level[i]);
      }
    }
    position_reddist[position_reds] += (*it).second;
  }

  // Berücksichtigung aller möglichen Rotminuten und Überführung in
  // Reihenstärken
  std::array<unsigned char, 5> base_array;
  std::copy_n(base.begin(), 5,  base_array.begin());
  // level_loss_db speichert zu Stärkenkombinationen ab, mit welcher
  // Wahrscheinlichkeit welche Stärke verloren wird
  std::unordered_map< std::multiset<unsigned char>,
                      std::unordered_map<unsigned char, double>,
                      boost::hash< std::multiset<unsigned char> > >
    level_loss_db;
  std::unordered_map< std::array<unsigned char, 5>,
                      double, boost::hash< std::array<unsigned char, 5> > >
    formationdist;
  for (std::unordered_map< std::array<std::multiset<unsigned char>, 5>, double,
       boost::hash< std::array<std::multiset<unsigned char>, 5> > >::iterator
         it = position_reddist.begin(); it != position_reddist.end();
         ++it) {
    std::array<std::multiset<unsigned char>, 5> position_reds = (*it).first;
    // zunächst werden die möglichen Stärkeverluste für die Reihen einzeln
    // berechnet
    std::array< std::unordered_map<unsigned char, double>, 5> all_level_losses;
    for (unsigned char i = 0; i < 5; i++) {
      std::multiset<unsigned char> redset = position_reds[i];
      std::unordered_map<unsigned char, double> level_losses;
      level_losses[0] = 1.0;
      std::multiset<unsigned char> level_losses_redset;
      // Für die Rotkombination der Reihe wird nachgeschaut, ob ihre
      // Stärkeverluste schon in level_loss_db gespeichert sind. Wenn nicht,
      // wird ein Element beiseitegeschoben und wieder geschaut.
      while (!redset.empty()) {
        std::multiset<unsigned char> new_redset;
        for (std::multiset<unsigned char>::iterator it_redset = redset.begin();
             ; ) {
          std::unordered_map<unsigned char, double> new_level_losses;
          try {
            new_level_losses = level_loss_db.at(redset);
          } catch (std::out_of_range &e) {
            if (redset.size() == 1) {
              // Berechnung der möglichen Stärkeverluste einer Spielerstärke
              unsigned char level = *it_redset;
              signed char minute = -1;
              unsigned char new_minute;
              for (unsigned char loss = 0; loss < level; loss++) {
                new_minute = static_cast<unsigned char>(round((loss + 0.5) *
                  90 / level * 100) / 100);
                new_level_losses[loss] = (new_minute - minute) / 90.0;
                minute = new_minute;
              }
              new_level_losses[level] = (89 - minute) / 90.0;
              level_loss_db[redset] = new_level_losses;
            } else {
              std::multiset<unsigned char>::iterator move_it = it_redset;
              ++it_redset;
              new_redset.insert(*move_it);
              redset.erase(move_it);
              continue;
            }
          }
          // Kombination mit bisherigen Stärkeverlusten
          std::unordered_map<unsigned char, double> res_level_losses;
          for (std::unordered_map<unsigned char, double>::iterator it_old =
               level_losses.begin(); it_old != level_losses.end(); ++it_old) {
            for (std::unordered_map<unsigned char, double>::iterator it_new =
                 new_level_losses.begin(); it_new != new_level_losses.end();
                 ++it_new) {
              res_level_losses[(*it_old).first + (*it_new).first] +=
                (*it_old).second * (*it_new).second;
            }
          }
          level_losses = res_level_losses;
          // Speicherung der Stärkeverluste
          level_losses_redset.insert(redset.begin(), redset.end());
          level_loss_db[level_losses_redset] = level_losses;
          redset = new_redset;
          break;
        }
      }
      all_level_losses[i] = level_losses;
    }
    // Überführung der Reihenstärkeverluste in Aufstellungen
    std::array<std::unordered_map<unsigned char, double>::iterator, 5> it_array;
    for (unsigned char i = 0; i < 5; i++) {
      it_array[i] = all_level_losses[i].begin();
    }
    for ( ; ; ) {
      std::array<unsigned char, 5> formation = base_array;
      double p = (*it).second;
      for (unsigned char i = 0; i < 5; i++) {
        formation[i] -= std::min((*it_array[i]).first, formation[i]);
        p *= (*it_array[i]).second;
      }
      formationdist[formation] += p;
      ++it_array[4];
      unsigned char i = 4;
      while (it_array[i] == all_level_losses[i].end()) {
        if (i == 0) goto endloop;
        it_array[i] = all_level_losses[i].begin();
        i--;
        ++it_array[i];
      }
    }
    endloop: ;
  }

  List out(formationdist.size());
  List::iterator lit = out.begin();
  for (std::unordered_map< std::array<unsigned char, 5>, double,
       boost::hash< std::array<unsigned char, 5> > >::iterator it =
         formationdist.begin(); it != formationdist.end(); ++it) {
    std::array<unsigned char, 5> intarray = (*it).first;
    std::vector<unsigned> intvec(intarray.begin(), intarray.end());
    *lit = List::create(_["formation"] = intvec, _["prob"] = (*it).second);
    ++lit;
  }
  return out;
}
