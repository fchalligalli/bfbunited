
R-Paket mit dem [United](https://de.wikipedia.org/wiki/United_(Spiel))
im BFB ausgewertet wird. Die Dokumentation ist weitgehend auf deutsch,
um der Amtssprache des BFB zu entsprechen.

# Installation

Mithilfe des R-Pakets `remotes` kann das Paket von gitlab installiert
werden. Auf Windows scheitert “System32/tar.exe”, die von `utils::untar`
aufgerufen wird jedoch beim korrekten Entpacken der Umlaute in den
Dateinamen der von gitlab heruntergeladenen “tar.gz”-Datei. Erfolgreich
ist hingegen GNU tar, das sowieso in Rtools enthalten ist. Damit es die
Windows-Dateipfade korrekt interpretiert, braucht es allerdings die
Option `--force-local`. Damit `utils::untar` das stattdessen verwendet,
kann einfach die Umgebungsvariable `TAR` entsprechend gesetzt werden, z.
B. so:

``` r
if (.Platform$OS.type == "windows") {
  Sys.setenv(TAR = "C:\\rtools43\\usr\\bin\\tar.exe --force-local")
}

remotes::install_gitlab("fchalligalli/bfbUnited", build_vignettes = TRUE)
```

Ohne Vignetten geht es etwas schneller:

``` r
remotes::install_gitlab("fchalligalli/bfbUnited")
```

Danach lässt sich das Paket mit `library("bfbUnited")` laden.

## Bestimmten Branch oder Version installieren

So wird beispielsweise die neuste Version des develop-Branches
installiert:

``` r
remotes::install_gitlab("fchalligalli/bfbUnited@develop", 
                        build_vignettes = TRUE)
```

Da alle Releases mit ihrer Versionsnummer getaggt sind, funktioniert das
Installieren einer Version genauso:

``` r
remotes::install_gitlab("fchalligalli/bfbUnited@43.0.0", 
                        build_vignettes = TRUE)
```

## Von lokalem Paket installieren

Um einen bestimmten älteren Commit zu installieren, kann dieser Commit
gepullt werden und das working directory auf den Projektordner gesetzt
werden. Dann funktioniert:

``` r
remotes::install_local(build_vignettes = TRUE)
```

# Dokumentation

Die vollständige Paketdokumentation ist mit `package?bfbUnited`
einsehbar.

# Versionierung

Die erste Versionsnummer steht für die Saison, die zweite für die Runde,
nach der der Release geschah. 0 bedeutet vor der ersten Runde. Die
dritte Versionsnummer ist für mehrere Releases während einer Runde und
zählt von 0 an.

# Vignetten

`vignette("Anleitung", "bfbUnited")` enthält eine kurze Anleitung zur
Verwendung des Paketes.

# Andere R-Pakete mit United-Bezug

David Schindler, Ex-Manager von Omega Virius, hat
[unitedR](https://cran.r-project.org/web/packages/unitedR/index.html)
auf CRAN veröffentlicht.
