57.0.0 (2024-11-18)
--------------------------------------------------------------------------------

 * Der Exponent des NL-Wertes in der Transaktionssteuer wird zum einstellbaren 
  Parameter mit Standardwert 1,5 (zuvor 2). Die Steuer darauf steigt von 
  0,125% (1/800) auf 2,5% (1/40).

56.0.0 (2024-07-12)
--------------------------------------------------------------------------------

 * readFile() an readODS-Update angepasst.

53.0.0 (2023-06-09)
--------------------------------------------------------------------------------

 * Transaktionssteuer wird auf den NL-Wert jedes gekauften Spielers einzeln 
  erhoben statt auf die Summe der NL-Werte. So wird die 
  Mindesttransaktionssteuer für jeden gekauften Spieler erhoben.
 * Die Transaktionssteuer wird auf den quadrierten NL-Wert erhoben. Ihr 
  Standardwert sinkt von 50% auf 0,125% (1/800).

52.0.0 (2023-02-11)
--------------------------------------------------------------------------------

 * Das Zugabgabeformular zeigt die Anzahl aufgestellter Spieler an.

51.1.0 (2022-10-17)
--------------------------------------------------------------------------------

 * Transfersperren verhindern keine Verkäufe in die Nichtliga.

51.0.0 (2022-10-05)
--------------------------------------------------------------------------------

 * Die Elfmeterwahrscheinlichkeit pro Härtepunkt wird jetzt 
  schiedsrichterspezifisch eingestellt. Die Elfmeterwahrscheinlichkeit 
  verschwindet daher aus der Parameterdatei. Die von prepareUnited() erzeugte 
  Beispieldatei "Disziplinarmatrix.ods" wird in "Härtestrafen.ods" umbenannt 
  und enthält jetzt auch die Elfmeterwahrscheinlichkeit. setCardMatrix(), 
  addCardMatrix() und delCardMatrix() heißen jetzt setEffortPenalties(), 
  addEffortPenalties() und delEffortPenalties().
 * Es sind auch mehrere Elfmeterwahrscheinlichkeiten pro Schiedsrichter möglich, 
  aus denen dann pro Spiel gleichverteilt gezogen wird. eGame() 
  berücksichtigt dies und berechnet die Spielausgangswahrscheinlichkeiten vor 
  der Ziehung.

50.12.0 (2022-08-09)
--------------------------------------------------------------------------------

 * addCompetition() für neue Runden gefixt. Zuvor wurde der bisherige 
  Spielplan gelöscht.

50.2.0 (2022-05-23)
--------------------------------------------------------------------------------

 * Spieler im TL-Angebot werden ohne die beim Verkauf entstehende Handelssperre 
  angezeigt.

50.0.0 (2022-04-24)
--------------------------------------------------------------------------------

 * Transaktionssteuer wird auf die Summe des NL-Wertes aller gekauften Spieler 
  erhoben anstatt auf das NL-Saldo des Handels.
 * Die Transferliste wird als separate csv-Tabelle anstatt als Text ausgegeben.
 * GM- und TL-Angebot werden als separate csv-Tabellen anstatt als Text 
  ausgegeben.
 * Die Eigenschaften "Tore" und "trainierbar bis" sind für GM-Spieler nicht mehr 
  setzbar, weil das nie gebraucht wird.
 * Die eingesetzte Härte wird in den geheimen Spielergebnisinformationen 
  angezeigt.
 * playGameRound() trägt nur die Spiele der nächsten Runde aus und lässt alle 
  nicht direkt damit zusammenhängenden Verfahren aus.
 * emayili kann wieder von CRAN installiert werden.
 * Alle speziellen Vorkehrungen, um UTF-8 auf Windows kompatibel zu machen, 
  wurden entfernt. Mit R 4.2.0 sollte UTF-8 standardmäßig funktionieren.

49.0.4 (2021-12-29)
--------------------------------------------------------------------------------

 * Fix in checkMove() nachgeholt.

49.0.3 (2021-12-24)
--------------------------------------------------------------------------------

 * setCompetitionOrder() legt eine Wettbewerbsrangfolge fest.
 * Für private Verkäufe abwärts der Wettbewerbsrangfolge wird eine 
  Transaktionssteuer fällig, die sich über die Parameterdatei einstellen lässt.
 * Spieler mit der neuen Eigenschaft "§" können nicht abwärts der 
  Wettbewerbsrangfolge verkauft werden.
 * Die Anzahl gleichzeitig einspielbarer neuer Talente wird durch die 
  entsprechende Einstellung in der Parameterdatei beschränkt. NMR-Aufstellungen 
  stellen in der Folge keinen uneingespielten Torwart auf, wenn dadurch 
  insgesamt weniger Spieler eingesetzt werden könnten.
 * Pull-Request gemergt: emayili kann wieder von Hauptrepo installiert werden.

49.0.2 (2021-12-14)
--------------------------------------------------------------------------------

 * sendTeamFiles() für Umlaute in Dateinamen gefixt, indem emayili-Fork 
  installiert wird.

49.0.1 (2021-12-12)
--------------------------------------------------------------------------------

 * sendTeamFiles() für emayili-Update gefixt.

49.0.0 (2021-12-05)
--------------------------------------------------------------------------------

 * Spiele können jetzt Schiedsrichter haben, die unterschiedliche 
  Disziplinarmatrizen einsetzen. Zusätzliche Disziplinarmatrizen lassen sich 
  mit addCardMatrix() hinzufügen. Schiedsrichter werden dann mit 
  addReferees() mit Namen und eingesetzter Disziplinarmatrix registriert. Der 
  Spielplan wird um eine optionale Spalte erweitert, in der der 
  Schiedsrichtername eines Spiels eingetragen werden kann.
 * sampleCompetitionReferees() verteilt Schiedsrichter zufällig auf einen 
  existierenden Wettbewerb.
 * setSchedule() überschreibt den Spielplan standardmäßig nur in der Zukunft.
  So lässt sich die Funktion auch innerhalb einer Saison anwenden, ohne 
  bisherige Spielergebnisse zu verlieren. writeSchedule() gibt den Spielplan 
  standardmäßig auch nur in der Zukunft aus. Das ist vor dem Überschreiben mit 
  setSchedule() aber keine Notwendigkeit, weil eingelesene Runden aus der 
  Vergangenheit einfach ignoriert würden.
 * Das Training in NMR-Zugabgaben wird den United-Regeln angepasst.
 * Bugfix in NMR-Zugabgabe: Das Einspielen neuer Talente wurde nicht richtig 
  gezählt.

48.2.0 (2021-08-28)
--------------------------------------------------------------------------------

 * Zweistelliger negativer Stärkebonus beim Altern wird korrekt eingelesen.

47.12.0 (2021-07-24)
--------------------------------------------------------------------------------

 * WP in 11: Die Stärke von Fs wird um 1 reduziert und tTs werden nicht mehr 
  berücksichtigt.

47.11.2 (2021-07-06)
--------------------------------------------------------------------------------

 * Bug aus 47.11.1: DP, WP und GE in richtiger Reihenfolge in Tabelle eintragen.

47.11.1 (2021-07-05)
--------------------------------------------------------------------------------

 * In Blitz- und Partialtabellen werden keine DP, WP und GE mehr angezeigt.

47.11.0 (2021-06-30)
--------------------------------------------------------------------------------

 * Die Minute von Elfmeterfehlschüssen in der Nachspielzeit wird korrekt 
  formatiert.

47.2.0 (2021-04-17)
--------------------------------------------------------------------------------

 * Bugfix in playGame(): Seit 46.12.0 wurde für jedes Amateurelfmetertor ein 
  weiteres Tor dieses Amateurs aus dem Spiel in den Toren aufgelistet. Das 
  Ergebnis war aber weiterhin korrekt.
 * Die Rundung der Erwartungstabellen muss auf 10 Nachkommastellen erhöht werden.

46.12.1 (2021-03-21)
--------------------------------------------------------------------------------

 * Benutzte Aufstellungen von NMR-Amateuren werden nicht mehr entfernt. Hier ist 
  also nur eine Aufstellung pro Runde pro Team möglich. Das ist konsistent mit 
  der kürzlich geänderten Behandlung von Amateuraufstellungen echter Teams.
 * writeRoundRobin() erlaubt den Namen des Wettbewerbs einzustellen. Die 
  Outputdatei wird standardmäßig nach ihm benannt.
 * Erwartungstabellen werden auf 11 Stellen nach dem Komma gerundet, sodass 
  gleiche Platzierungen korrekt erkannt werden.
 * Fix: Eigentore nicht in Teamdateien mitzählen.

46.12.0 (2021-03-21)
--------------------------------------------------------------------------------

 * Zu Elfmeterfehlschüssen werden Minuten und Schützen ausgelost. Die Minuten 
  sind unabhängig von allen Spielereignissen.
 * Zu einer in der Parameterdatei einstellbaren Wahrscheinlichkeit 
  (Standard: 2,5%) werden aus dem Spiel geschossene Tore zu Eigentoren. Die 
  Torminute wird weiterhin durch den Torschützen bestimmt, dem das Tor 
  eigentlich zugelost war. Eigentorschütze wird ein zufälliger Spieler der 
  gegnerischen Mannschaft, der zur Minute auf dem Platz steht.
 * Automatische Teamdateimails erhalten einen leeren Text, damit bestimmte 
  E-Mail Clients die txt-Datei im Anhang nicht spoilern.
 * Naming: Die unbedingten Erwartungstabellen heißen einfach nur noch 
  Erwartungstabellen, während die auf verteilte rote Karten und ausgeführte 
  Elfmeter bedingten jetzt Erwartungstabellen nach Härtestrafen heißen. In der 
  Tabellenausgabe von writeTable() stehen die normalen Erwartungstabellen 
  jetzt vor denen nach Härtestrafen.

46.1.0 (2020-12-05)
--------------------------------------------------------------------------------

 * Kommas in Spielernamen werden nicht zugelassen.
 * writeTxtFile() erzeugt stets UTF-8 Output.

45.12.0 (2020-11-15)
--------------------------------------------------------------------------------

 * Pro Runde hat jedes Team nur noch eine Amateuraufstellung, die für alle 
  Amateurspiele verwendet wird.

45.11.0 (2020-10-24)
--------------------------------------------------------------------------------

 * Fix: Auswärts eingesetzte MP werden in den unbedingten 
  Spielausgangswahrscheinlichkeiten berücksichtigt.

45.9.0 (2020-10-01)
--------------------------------------------------------------------------------

 * Nicht-k.-o.-Spiele haben eine zufällige ganzzahlige Spielzeit von 90 bis 95 
  Minuten. In der Nachspielzeit können Tore fallen, auf sonstige Vorgänge wie 
  rote Karten hat sie aber keine Auswirkung.

45.0.2 (2020-06-30)
--------------------------------------------------------------------------------

 * Automatische E-Mails weiter verbessern.

45.0.1 (2020-06-27)
--------------------------------------------------------------------------------

 * Automatisch versendete E-Mails verbessern.

45.0.0 (2020-06-26)
--------------------------------------------------------------------------------

 * Feldreihen können auch in der Verlängerung nicht unter 0 reduziert werden.
  Zuvor war dies durch eine Kombination aus 3:1-Verletzung und roten Karten 
  möglich.
 * Nach dem Altern werden die Spieler sortiert.

44.12.0 (2020-06-17)
--------------------------------------------------------------------------------

Neue Features:

 * sendTeamFiles() verschickt alle Teamdateien per E-Mail.
 * writeTable() bekommt Parameter für Blitz- und Partialtabellen.
 * writeNMR() erhält additional.pro.games.HV und additional.ama.games.WP, 
  womit Aufstellungen für Spiele erstellt werden können, die noch nicht im 
  Spielplan stehen.
 * checkMove() prüft eine Zugabgabe auf mögliche Fehler. checkAllMoves() 
  nimmt sich alle Zugabgaben der nächsten Runde vor. Diese Funktionen haben 
  einen rein investigativen Zweck und sind nicht in die Auswertung eingebunden.

Fixes:

 * NMR-Amateure werden bei der Tabellenerstellung korrekt berücksichtigt.
 * Amateuraufstellungen werden auf die 3:1-Regel und einen Torwart und Ausputzer 
  nicht stärker als 10 geprüft.
 * MP-Einsatz auf den Wettbewerb, der im Element competition eines Teams 
  eingetragen ist, beschränkt. Vorher war der Einsatz in allen nicht-KO-Spielen 
  zugelassen.
 * Mehrfaches Training eines Spielers in einer Runde wird verhindert.

Entfernt:

 * trainPlayer() entfernt. playRound() führt Training stattdessen direkt 
  durch.

44.11.0 (2020-05-26)
--------------------------------------------------------------------------------

 * Bugfix in Code für Amateurtorwart.

44.3.2 (2020-03-31)
--------------------------------------------------------------------------------

 * Elfmetertore in Spielerstatistiken mitzählen.

44.3.1 (2020-03-31)
--------------------------------------------------------------------------------

 * Tore sind kein Spielerattribut mehr und werden für die entsprechenden 
  Spielerdarstellungen immer neu berechnet. Dazu wird die Ligazugehörigkeit des 
  Teams herangezogen, die ein neues Teamattribut ist und in der Teamdatei im 
  Feld E1 eingetragen wird. In der Torschützenliste werden 
  Team-Spieler-Kombinationen gelistet, ein gewechselter Spieler kann also 
  mehrfach vorkommen.

44.3.0 (2020-03-29)
--------------------------------------------------------------------------------

 * Weiteren Bug in Erwartungstabellenberechnung fixen.

44.2.1 (2020-03-23)
--------------------------------------------------------------------------------

 * Erwartungstabellen korrekt anzeigen.

44.2.0 (2020-03-16)
--------------------------------------------------------------------------------

 * Verschossene Heimelfmeter wirklich korrekt anzeigen.

44.1.1 (2020-03-04)
--------------------------------------------------------------------------------

 * Verschossene Heimelfmeter korrekt anzeigen.

44.1.0 (2020-03-03)
--------------------------------------------------------------------------------

 * Tabellenüberschriften fixen.

44.0.1 (2020-03-03)
--------------------------------------------------------------------------------

 * Falschen Schuldenalarm wegen Vorzeichenfehler beheben.

44.0.0 (2020-02-21)
--------------------------------------------------------------------------------

 * boundedSumGrid() in C++ auslagern.
 * writeTeamTxtFiles() zu writeTeamFiles(mode = "txt") ändern.
 * Numerische Rundungsfehler unterbinden, durch die bei einer roten Karte ein 
  Stärkepunkt zu viel entfernt werden konnte.
 * Kompatibilität mit xls- und xlsx-Dateien entfernen.
 * Unbedingte Erwartungstabellen implementieren.
 * Bei Fehlern beim Einlesen der Zugabgabe den Teamnamen anzeigen.
 * Hilfreiche Fehlermeldung bei fehlender Aufstellung.
 * playRound() bekommt das Argument team.file.formats, durch das die 
  Teamdateiformate spezifiziert werden können.
 * Kurzergebnisliste in Rundendatei drucken.
 * Eigene Fehlermeldung für ungerade Härte auf Torwart oder Ausputzer, anstatt 
  nur zu viel Härte anzuzeigen.

43.0.1 (2019-10-22)
--------------------------------------------------------------------------------

 * Installationsanleitung einen Patch für Windows hinzufügen.
 * writeTeamTxtFiles() hinzufügen.
 * Spielereigenschaften im privaten Handel enthüllen.

43.0.0 (2019-10-16)
--------------------------------------------------------------------------------

Erster Release des United-Auswertungspakets in Form eines R-Pakets. Der Code 
wurde komplett überarbeitet und insbesondere auf data.table umgestellt.
