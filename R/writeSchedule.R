#' Spiel ausgeben
#'
#' Übersetzt die Programmdarstellung einer Spielinfo in ihre textuelle
#' Darstellung.
#'
#' @author Philipp Hallmeier
#' @param info einzeilige `data.table`. Spalten wie im Rückgabewert von
#'             [readGame()].
#' @return Einzeilige `data.table`.
formatGameInfo <- function(info){
  assertDataTable(info, nrows = 1)

  return(data.table(D = info$Home, E = info$Away, F = as.character(info$HV),
                    G = as.character(info$Ama.WP), H = if (info$Is.ko) {
                      "ja"
                    } else {
                      "nein"
                    }, I = paste(vapply(info[, .(WP.w, WP.d, WP.l)],
                                        formatC, "", digits = 1, format = "f",
                                        dec = ","), collapse = " "),
                    J = paste(info$GE.w, info$GE.d, info$GE.l),
                    K = info$Referee))
}

#' Spieltag ausgeben
#'
#' Übersetzt die Programmdarstellung eines Spieltags in seine textuelle
#' Darstellung.
#'
#' @author Philipp Hallmeier
#' @param matchday Liste mit Elementen wie im Rückgabewert von [readMatchday()].
#' @return `data.table`.
#' @seealso [formatGameInfo()]
formatMatchday <- function(matchday){
  assertList(matchday)

  fmatchday <- data.table(C = c(matchday$name,
                                character(length(matchday$games) - 1L)),
                          rbindlist(lapply(matchday$games, function(game){
                            formatGameInfo(game$info)
                          })))
  return(fmatchday)
}

#' Wettbewerb ausgeben
#'
#' Übersetzt die Programmdarstellung eines Wettbewerbs in seine textuelle
#' Darstellung.
#'
#' @author Philipp Hallmeier
#' @param competition Liste mit Elementen wie im Rückgabewert von
#'                    [readCompetition()].
#' @return `data.table`.
#' @seealso [formatMatchday()], [formatGameInfo()]
formatCompetition <- function(competition){
  assertList(competition)

  fcompetition <- rbindlist(lapply(competition$matchdays, formatMatchday))
  fcompetition <- data.table(B = c(competition$name,
                                   character(nrow(fcompetition) - 1L)),
                             fcompetition)
  return(fcompetition)
}

#' Runde ausgeben
#'
#' Übersetzt die Programmdarstellung einer Runde in ihre textuelle Darstellung.
#'
#' @author Philipp Hallmeier
#' @param round Liste mit Elementen wie im Rückgabewert von [readRound()].
#' @return `data.table`.
#' @seealso [formatCompetition()], [formatMatchday()], [formatGameInfo()]
formatRound <- function(round){
  assertList(round)

  fround <- rbindlist(lapply(round$competitions, formatCompetition))
  fround <- data.table(A = c(as.character(round$number),
                             character(nrow(fround) - 1L)), fround)
  return(fround)
}

#' Spielplan in eine csv-Datei schreiben
#'
#' Schreibt United-Spielplan in eine csv-Datei, wo dieser dann zum
#' Wiedereinlesen mit [setSchedule()] bearbeitet werden kann.
#'
#' @author Philipp Hallmeier
#' @template unitedIn
#' @inherit writeFile params return
#' @param full `logical(1)`. Ob der komplette Spielplan ausgegeben werden soll.
#'   Standardmäßig wird nur der noch ausstehende Spielplan ausgegeben.
#' @seealso [formatRound()], [formatCompetition()], [formatMatchday()],
#'   [formatGameInfo()]
#' @export
writeSchedule <- function(united, file, full = FALSE){
  assertUnited(united)
  assertString(file)

  schedule <- united$schedule
  if (!full) {
    round.number <- attr(united, "round")
    schedule.round.numbers <- vapply(united$schedule, `[[`, 1L, "number")
    schedule <- schedule[schedule.round.numbers >= round.number]
  }
  fschedule <- data.table(A = "Runde", B = "Wettbewerb", C = "Wettbewerbsrunde",
                          D = "Heim", E = "Auswärts", F = "HV",
                          G = "Amateur-WP", H = "KO-Spiel", I = "WP S U N",
                          J = paste(attr(united, "currency"), "S U N"),
                          K = "Schiedsrichter")
  fschedule <- rbindlist(c(list(fschedule), lapply(schedule, formatRound)))
  if (fschedule[-1, all(K == "")]) fschedule[, K := NULL]
  writeFile(fschedule, file)
}
