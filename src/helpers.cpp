#include <Rcpp.h>
using namespace Rcpp;

double fac(unsigned char x) {
  double res = 1.0;
  for (unsigned char i = 2; i <= x; i++) res *= i;
  return res;
}

unsigned ifac(unsigned char x) {
  unsigned res = 1;
  for (unsigned char i = 2; i <= x; i++) res *= i;
  return res;
}

double prod(NumericVector x) {
  double res = 1;
  for (NumericVector::iterator it = x.begin(); it != x.end(); ++it) {
    res *= *it;
  }
  return res;
}

double powprod(NumericVector x, IntegerVector y) {
  double res = 1;
  for (unsigned char i = 0; i < x.size(); i++) {
    res *= pow(x[i], y[i]);
  }
  return res;
}
