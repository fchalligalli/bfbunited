#' Parameter setzen
#'
#' Setzt United-Parameter auf Werte aus einer Datei.
#'
#' @author Philipp Hallmeier
#' @template unitedInOut
#' @param file `character(1)`. Standardwert `"Parameter.ods"`.
#' @export
setParameters <- function(united, file = "Parameter.ods"){
  assertUnited(united)
  assertString(file)

  parameters <- readFile(file)[-1, B]
  attr(united, "WP.base") <- as.double(sub(",", ".", parameters[1]))
  attr(united, "WP.max") <- as.double(sub(",", ".", parameters[2]))
  attr(united, "WP.warn") <- parameters[3] == "ja"
  attr(united, "GE.base") <- as.integer(parameters[4])
  attr(united, "interest") <- as.double(sub(",", ".", parameters[5]))
  attr(united, "currency") <- parameters[6]
  attr(united, "DP.yellow") <- as.integer(parameters[7])
  if (attr(united, "DP.yellow") < 0) stop("Negative DP für gelbe Karten")
  attr(united, "tradelock") <- as.integer(parameters[8])
  if (attr(united, "tradelock") < 0) stop("Negative Handelssperre eingestellt")
  attr(united, "downsell.tax.e") <- as.double(sub(",", ".", parameters[9]))
  attr(united, "downsell.tax") <- as.double(sub(",", ".", parameters[10])) / 100
  if (attr(united, "downsell.tax") > 1 || attr(united, "downsell.tax") < 0) {
    stop("ungültige Verkaufsgebühr")
  }
  attr(united, "downsell.tax.min") <- as.integer(parameters[11])
  attr(united, "b") <- as.integer(parameters[12])
  attr(united, "a") <- eval(parse(text = parameters[13]))
  attr(united, "q") <- as.double(sub(",", ".", parameters[14]))
  attr(united, "p") <- as.double(sub(",", ".", parameters[15]))
  attr(united, "max.practice") <- as.integer(parameters[16])
  if (attr(united, "max.practice") < 2) {
    stop("Einspiellimit von weniger als 2 derzeit nicht unterstützt")
  }
  attr(united, "games.practice") <- as.integer(parameters[17])
  if (attr(united, "games.practice") < 1) {
    "Spielanzahl zum Einspielen muss positiv sein"
  }
  attr(united, "games.exp") <- as.integer(parameters[18])
  if (attr(united, "games.exp") < 1) {
    "Spielanzahl für Reihenqualifikationen muss positiv sein"
  }
  attr(united, "owngoalprob") <- as.double(sub(",", ".", parameters[19])) / 100
  if (attr(united, "owngoalprob") > 1 || attr(united, "owngoalprob") < 0) {
    stop("ungültige Eigentorwahrscheinlichkeit")
  }
  attr(united, "MP.max") <- as.integer(parameters[20])
  return(united)
}
