#ifndef __boundedSumGrid__
#define __boundedSumGrid__
using namespace Rcpp;

IntegerMatrix boundedSumGrid(List args, int lb, int ub, IntegerVector w);

#endif
