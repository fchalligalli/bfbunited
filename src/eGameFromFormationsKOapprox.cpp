#include <Rcpp.h>
#include "calcPlaygoaldist.h"
#include "helpers.h"
#include <boost/container_hash/hash.hpp>
using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::depends(BH)]]
// [[Rcpp::export(rng=false, name="C_eGameFromFormationsKOapprox")]]
List eGameFromFormationsKOapprox(List home, List away,
                                 unsigned char home_effort,
                                 unsigned char away_effort,
                                 NumericVector penaltyprobs) {
  long long unsigned ops = 0;

  // List in unordered_map schreiben
  std::unordered_map<std::array<unsigned char, 10>, double,
                     boost::hash< std::array<unsigned char, 10> > >
    home_fdist(home.size());
  for (List::iterator it = home.begin(); it != home.end(); ++it) {
    List x = *it;
    std::vector<unsigned char> fvec = x["formation"];
    std::array<unsigned char, 10> formation;
    std::copy_n(fvec.begin(), 10, formation.begin());
    double prob = x["prob"];
    home_fdist[formation] += prob;
  }
  std::unordered_map< std::array<unsigned char, 10>, double,
                      boost::hash< std::array<unsigned char, 10> > >
    away_fdist(away.size());
  for (List::iterator it = away.begin(); it != away.end(); ++it) {
    List x = *it;
    std::vector<unsigned char> fvec = x["formation"];
    std::array<unsigned char, 10> formation;
    std::copy_n(fvec.begin(), 10, formation.begin());
    double prob = x["prob"];
    away_fdist[formation] += prob;
  }

  // Kombination der Aufstellungen zu Begegnungen, die durch Chancen und
  // Hintermannschaften identifiziert werden
  long long unsigned ngames = home_fdist.size() * away_fdist.size();
  bool manygames = (ngames > 10000000);
  if (manygames) {
    Rprintf("Berechne Chancen für %llu Spiele...\n", ngames);
    Rprintf("\rFortschritt: %6.2f%%", 0.0);
  }
  double p_skip = 0.0;
  double skipped_p = 0.0;
  bool skip = (ngames > 1000000000);
  if (skip) {
    if (ngames > 10000000000) {
      p_skip = 0.01 / ngames;
    } else {
      p_skip = 0.001 / ngames;
    }
  }
  std::unordered_map< std::array<unsigned char, 12>, double,
                      boost::hash< std::array<unsigned char, 12> > >
    attemptdist(ngames / 8);
  for (std::unordered_map< std::array<unsigned char, 10>, double,
       boost::hash< std::array<unsigned char, 10> > >::iterator
         it_home = home_fdist.begin(); it_home != home_fdist.end(); ++it_home) {
    std::array<unsigned char, 10> fhome = (*it_home).first;
    double phome = (*it_home).second;
    for (std::unordered_map< std::array<unsigned char, 10>, double,
         boost::hash< std::array<unsigned char, 10> > >::iterator
           it_away = away_fdist.begin(); it_away != away_fdist.end();
           ++it_away) {
      double p = phome * (*it_away).second;
      if (p < p_skip) {
        skipped_p += p;
      } else {
        std::array<unsigned char, 10> faway = (*it_away).first;

        unsigned char home_v_attempts = 0;
        unsigned char home_m_attempts = 0;
        unsigned char home_s_attempts = 0;
        unsigned char away_v_attempts = 0;
        unsigned char away_m_attempts = 0;
        unsigned char away_s_attempts = 0;
        if (fhome[2] > faway[4]) {
          home_v_attempts = (fhome[2] - faway[4] + 2) / 4;
        } else {
          if (faway[4] > fhome[2] + fhome[1]) {
            away_s_attempts = faway[4] - fhome[2] - fhome[1];
          }
        }
        if (faway[2] > fhome[4]) {
          away_v_attempts = (faway[2] - fhome[4] + 2) / 4;
        } else {
          if (fhome[4] > faway[2] + faway[1]) {
            home_s_attempts = fhome[4] - faway[2] - faway[1];
          }
        }
        if (fhome[3] > faway[3]) {
          home_m_attempts = (fhome[3] - faway[3] + 1) / 2;
        } else {
          away_m_attempts = (faway[3] - fhome[3] + 1) / 2;
        }
        unsigned char home_attempts = home_v_attempts + home_m_attempts +
          home_s_attempts;
        unsigned char away_attempts = away_v_attempts + away_m_attempts +
          away_s_attempts;

        home_v_attempts = 0;
        home_m_attempts = 0;
        home_s_attempts = 0;
        away_v_attempts = 0;
        away_m_attempts = 0;
        away_s_attempts = 0;
        if (fhome[7] > faway[9]) {
          home_v_attempts = ((fhome[7] - faway[9] + 2) / 4 + 1) / 3;
        } else {
          if (faway[9] > fhome[7] + fhome[6]) {
            away_s_attempts = (faway[9] - fhome[7] - fhome[6] + 1) / 3;
          }
        }
        if (faway[7] > fhome[9]) {
          away_v_attempts = ((faway[7] - fhome[9] + 2) / 4 + 1) / 3;
        } else {
          if (fhome[9] > faway[7] + faway[6]) {
            home_s_attempts = (fhome[9] - faway[7] - faway[6] + 1) / 3;
          }
        }
        if (fhome[8] > faway[8]) {
          home_m_attempts = ((fhome[8] - faway[8] + 1) / 2 + 1) / 3;
        } else {
          away_m_attempts = ((faway[8] - fhome[8] + 1) / 2 + 1) / 3;
        }
        unsigned char home_et_attempts = home_v_attempts + home_m_attempts +
          home_s_attempts;
        unsigned char away_et_attempts = away_v_attempts + away_m_attempts +
          away_s_attempts;

        attemptdist[std::array<unsigned char, 12>({
          home_attempts, faway[0], faway[1],
          away_attempts, fhome[0], fhome[1],
          home_et_attempts, faway[5], faway[6],
          away_et_attempts, fhome[5], fhome[6]})] += p;
      }
      ops++;
      if (ops % 1000000 == 0) {
        Rcpp::checkUserInterrupt();
        if (manygames) {
          Rprintf("\rFortschritt: %6.2f%%", (ops * 100.0) / ngames);
        }
      }
    }
  }
  if (manygames) {
    Rprintf("\rFortschritt: %6.2f%%\n", 100.0);
    if (skip) {
      Rprintf("max. Genauigkeitsverlust: %.4f%%\n", skipped_p * 100.0);
    }
  }

  // Initialisierung einiger Zwischenergebnisdatenbanken
  std::unordered_map< std::pair<unsigned char, unsigned char>,
                      std::vector<double>,
                      boost::hash< std::pair<unsigned char, unsigned char> > >
    playgoaldist_db;  // key: attempts, (14 - opn. T) * (15 - opn. A)

  // Getrennte Heim-/Auswärtsdatenbanken wegen anderer gegn. Härte:
  unsigned home_penalty_nfac = fac(away_effort);
  std::vector<double> home_penalty_bicos(away_effort + 1);
  for (unsigned char k = 0; k <= away_effort; k++) {
    home_penalty_bicos[k] = home_penalty_nfac / (fac(k) * fac(away_effort - k));
  }
  std::unordered_map< double, std::vector<double> > home_penaltydist_db;
  // ^ key: penaltygoalprob per effort
  std::unordered_map< std::tuple<unsigned char, unsigned char, double>,
                      std::vector<double>,
                      boost::hash< std::tuple<unsigned char, unsigned char,
                                              double> > >
    home_goaldist_db;
  // ^ key: attempts, (14 - opn. T) * (15 - opn. A), penaltygoalprob

  unsigned away_penalty_nfac = fac(home_effort);
  std::vector<double> away_penalty_bicos(home_effort + 1);
  for (unsigned char k = 0; k <= home_effort; k++) {
    away_penalty_bicos[k] = away_penalty_nfac / (fac(k) * fac(home_effort - k));
  }
  std::unordered_map< double, std::vector<double> > away_penaltydist_db;
  std::unordered_map< std::tuple<unsigned char, unsigned char, double>,
                      std::vector<double>,
                      boost::hash< std::tuple<unsigned char, unsigned char,
                                              double> > >
    away_goaldist_db;

  std::unordered_map< std::array<unsigned char, 6>, std::array<double, 5>,
                      boost::hash< std::array<unsigned char, 6> > > nt_db;
  std::unordered_map< std::array<unsigned char, 6>, std::array<double, 2>,
                      boost::hash< std::array<unsigned char, 6> > > et_db;
  std::unordered_map< std::array<unsigned char, 2>, std::array<double, 2>,
                      boost::hash< std::array<unsigned char, 2> > > ps_db;

  // Auswertung aller möglichen Spiele
  double egoals_home = 0.0;
  double egoals_away = 0.0;
  double p_home = 0.0;
  double p_away = 0.0;
  unsigned n_penaltyprobs = penaltyprobs.size();
  for (std::unordered_map< std::array<unsigned char, 12>, double,
       boost::hash< std::array<unsigned char, 12> > >::iterator
         it = attemptdist.begin(); it != attemptdist.end(); ++it) {
    std::array<unsigned char, 12> attemptvec = (*it).first;
    double prob = (*it).second;
    std::array<unsigned char, 6> nt_attemptvec;
    std::copy_n(attemptvec.begin(), 6, nt_attemptvec.begin());
    std::array<double, 5> nt_result;
    try {
      nt_result = nt_db.at(nt_attemptvec);
    } catch (std::out_of_range &u) {
      std::pair<unsigned char, unsigned char> home_playgoaldist_key
      (attemptvec[0], (14 - attemptvec[1]) * (15 - attemptvec[2]));
      std::pair<unsigned char, unsigned char> away_playgoaldist_key
        (attemptvec[3], (14 - attemptvec[4]) * (15 - attemptvec[5]));

      double game_egoals_home = 0.0;
      double game_egoals_away = 0.0;
      double pgame_home = 0.0;
      double pgame_draw = 0.0;
      double pgame_away = 0.0;
      for (NumericVector::iterator it_penaltyprobs = penaltyprobs.begin();
           it_penaltyprobs != penaltyprobs.end(); ++it_penaltyprobs) {
        double penaltyprob = *it_penaltyprobs;

        double home_penaltygoalprob = penaltyprob *
          (1.0 - attemptvec[1] / 20.0);
        std::tuple<unsigned char, unsigned char, double> home_goaldist_key
          (home_playgoaldist_key.first, home_playgoaldist_key.second,
           home_penaltygoalprob);
        std::vector<double> home_goaldist(attemptvec[0] + away_effort + 1);
        try {
          home_goaldist = home_goaldist_db.at(home_goaldist_key);
        } catch (std::out_of_range &e) {
          std::vector<double> home_playgoaldist(attemptvec[0] + 1);
          try {
            home_playgoaldist = playgoaldist_db.at(home_playgoaldist_key);
          } catch (std::out_of_range &e2) {
            home_playgoaldist = calcPlaygoaldist(home_playgoaldist_key);
            playgoaldist_db[home_playgoaldist_key] = home_playgoaldist;
          }
          std::vector<double> home_penaltydist(away_effort + 1);
          try {
            home_penaltydist = home_penaltydist_db.at(home_penaltygoalprob);
          } catch (std::out_of_range &e3) {
            double p = home_penaltygoalprob;
            double q = 1.0 - p;
            for (unsigned char k = 0; k <= away_effort; k++) {
              home_penaltydist[k] = home_penalty_bicos[k] * pow(p, k) *
                pow(q, away_effort - k);
            }
            home_penaltydist_db[home_penaltygoalprob] = home_penaltydist;
          }
          for (unsigned char i = 0; i <= away_effort; i++) {
            for (unsigned char j = 0; j <= attemptvec[0]; j++) {
              home_goaldist[i + j] += home_penaltydist[i] *
                home_playgoaldist[j];
            }
          }
          home_goaldist_db[home_goaldist_key] = home_goaldist;
        }

        double away_penaltygoalprob = penaltyprob *
          (1.0 - attemptvec[4] / 20.0);
        std::tuple<unsigned char, unsigned char, double> away_goaldist_key
          (away_playgoaldist_key.first, away_playgoaldist_key.second,
           away_penaltygoalprob);
        std::vector<double> away_goaldist(attemptvec[3] + home_effort + 1);
        try {
          away_goaldist = away_goaldist_db.at(away_goaldist_key);
        } catch (std::out_of_range &e) {
          std::vector<double> away_playgoaldist(attemptvec[4] + 1);
          try {
            away_playgoaldist = playgoaldist_db.at(away_playgoaldist_key);
          } catch (std::out_of_range &e2) {
            away_playgoaldist = calcPlaygoaldist(away_playgoaldist_key);
            playgoaldist_db[away_playgoaldist_key] = away_playgoaldist;
          }
          std::vector<double> away_penaltydist(home_effort + 1);
          try {
            away_penaltydist = away_penaltydist_db.at(away_penaltygoalprob);
          } catch (std::out_of_range &e3) {
            double p = away_penaltygoalprob;
            double q = 1.0 - p;
            for (unsigned char k = 0; k <= home_effort; k++) {
              away_penaltydist[k] = away_penalty_bicos[k] * pow(p, k) *
                pow(q, home_effort - k);
            }
            away_penaltydist_db[away_penaltygoalprob] = away_penaltydist;
          }
          for (unsigned char i = 0; i <= home_effort; i++) {
            for (unsigned char j = 0; j <= attemptvec[3]; j++) {
              away_goaldist[i + j] += away_penaltydist[i] *
                away_playgoaldist[j];
            }
          }
          away_goaldist_db[away_goaldist_key] = away_goaldist;
        }

        for (unsigned char i = 0; i < home_goaldist.size(); i++) {
          game_egoals_home += i * home_goaldist[i];
        }
        for (unsigned char i = 0; i < away_goaldist.size(); i++) {
          game_egoals_away += i * away_goaldist[i];
        }

        unsigned char a_end = away_goaldist.size() - 1;
        for (unsigned char h = 0; h < home_goaldist.size(); h++) {
          for (unsigned char a = 0; a < h; a++) {
            pgame_home += home_goaldist[h] * away_goaldist[a];
            if (a == a_end) goto endloop;
          }
          pgame_draw += home_goaldist[h] * away_goaldist[h];
          for (unsigned char a = h + 1; a <= a_end; a++) {
            pgame_away += home_goaldist[h] * away_goaldist[a];
          }
          endloop: ;
        }
      }

      pgame_home /= n_penaltyprobs;
      pgame_draw /= n_penaltyprobs;
      pgame_away /= n_penaltyprobs;
      game_egoals_home /= n_penaltyprobs;
      game_egoals_away /= n_penaltyprobs;

      nt_result = {pgame_home, pgame_draw, pgame_away,
                   game_egoals_home, game_egoals_away};
      nt_db[nt_attemptvec] = nt_result;
    }
    egoals_home += nt_result[3] * prob;
    egoals_away += nt_result[4] * prob;

    // Verlängerung
    std::array<unsigned char, 6> et_attemptvec;
    std::copy_n(attemptvec.begin() + 6, 6, et_attemptvec.begin());
    std::array<double, 2> et_result;
    try {
      et_result = et_db.at(et_attemptvec);
    } catch (std::out_of_range &e) {
      unsigned char T_away = et_attemptvec[1];
      unsigned char T_home = et_attemptvec[4];
      std::pair<unsigned char, unsigned char> home_playgoaldist_key
        (et_attemptvec[0], (14 - T_away) * (15 - et_attemptvec[2]));
      std::pair<unsigned char, unsigned char> away_playgoaldist_key
        (et_attemptvec[3], (14 - T_home) * (15 - et_attemptvec[5]));

      std::vector<double> home_goaldist(et_attemptvec[0] + 1);
      try {
        home_goaldist = playgoaldist_db.at(home_playgoaldist_key);
      } catch (std::out_of_range &e2) {
        home_goaldist = calcPlaygoaldist(home_playgoaldist_key);
        playgoaldist_db[home_playgoaldist_key] = home_goaldist;
      }
      std::vector<double> away_goaldist(et_attemptvec[3] + 1);
      try {
        away_goaldist = playgoaldist_db.at(away_playgoaldist_key);
      } catch (std::out_of_range &e2) {
        away_goaldist = calcPlaygoaldist(away_playgoaldist_key);
        playgoaldist_db[away_playgoaldist_key] = away_goaldist;
      }

      double p_et_home = 0.0;
      double p_et_draw = 0.0;
      double p_et_away = 0.0;
      unsigned char a_end = away_goaldist.size() - 1;
      for (unsigned char h = 0; h < home_goaldist.size(); h++) {
        for (unsigned char a = 0; a < h; a++) {
          p_et_home += home_goaldist[h] * away_goaldist[a];
          if (a == a_end) goto endloop_et;
        }
        p_et_draw += home_goaldist[h] * away_goaldist[h];
        for (unsigned char a = h + 1; a <= a_end; a++) {
          p_et_away += home_goaldist[h] * away_goaldist[a];
        }
        endloop_et: ;
      }

      if ((T_home == 0) & (T_away == 0)) {
        T_home = 10;
        T_away = 10;
      }
      std::array<double, 2> ps_result;
      std::array<unsigned char, 2> T_array = {T_home, T_away};
      try {
        ps_result = ps_db.at(T_array);
      } catch (std::out_of_range &e2) {
        std::array<double, 6> home_ps_goaldist;
        double efficiency_home = 1.0 - T_away / 20.0;
        double q_home = 1.0 - efficiency_home;
        for (unsigned char k = 0; k <= 5; k++) {
          home_ps_goaldist[k] = 120.0 / (fac(k) * fac(5 - k)) *
            pow(efficiency_home, k) * pow(q_home, 5 - k);
        }
        std::array<double, 6> away_ps_goaldist;
        double efficiency_away = 1.0 - T_home / 20.0;
        double q_away = 1.0 - efficiency_away;
        for (unsigned char k = 0; k <= 5; k++) {
          away_ps_goaldist[k] = 120.0 / (fac(k) * fac(5 - k)) *
            pow(efficiency_away, k) * pow(q_away, 5 - k);
        }

        double p_ps_home = 0.0;
        double p_ps_draw = 0.0;
        double p_ps_away = 0.0;
        for (unsigned char h = 0; h <= 5; h++) {
          for (unsigned char a = 0; a < h; a++) {
            p_ps_home += home_ps_goaldist[h] * away_ps_goaldist[a];
          }
          p_ps_draw += home_ps_goaldist[h] * away_ps_goaldist[h];
          for (unsigned char a = h + 1; a <= 5; a++) {
            p_ps_away += home_ps_goaldist[h] * away_ps_goaldist[a];
          }
        }

        double sd_home = efficiency_home * q_away;
        double sd_away = efficiency_away * q_home;
        double sd_end = sd_home + sd_away;
        p_ps_home += p_ps_draw * (sd_home / sd_end);
        p_ps_away += p_ps_draw * (sd_away / sd_end);

        ps_result = {p_ps_home, p_ps_away};
        ps_db[T_array] = ps_result;
      }

      p_et_home += p_et_draw * ps_result[0];
      p_et_away += p_et_draw * ps_result[1];

      et_result = {p_et_home, p_et_away};
      et_db[et_attemptvec] = et_result;
    }

    p_home += (nt_result[0] + nt_result[1] * et_result[0]) * prob;
    p_away += (nt_result[2] + nt_result[1] * et_result[1]) * prob;

    ops++;
    if (ops % 1000000 == 0) Rcpp::checkUserInterrupt();
  }

  double skipped_q = 1 - skipped_p;
  egoals_home /= skipped_q;
  egoals_away /= skipped_q;
  p_home /= skipped_q;
  p_away /= skipped_q;

  NumericVector egoals = NumericVector::create(
    _["home"] = egoals_home,
    _["away"] = egoals_away);
  NumericVector eresult = NumericVector::create(_["home"] = p_home,
                                                _["draw"] = 0.0,
                                                _["away"] = p_away);
  List out = List::create(_["e.goals"] = egoals, _["e.result"] = eresult);
  return out;
}
