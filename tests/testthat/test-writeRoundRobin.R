test_that("writeRoundRobin einen einlesbaren Spielplan produziert", {
  united <- startUnited(parameter.file = NULL, effort.penalties.file = NULL)
  united$teams <- lapply(letters[1:5], function(l) list(name = l)) # Dummy Teams
  tmp <- tempfile(fileext = ".csv")
  on.exit(unlink(tmp))
  expect_silent(writeRoundRobin(letters[1:3], rounds = 1, file = tmp))
  expect_silent(writeRoundRobin(letters[1:4], rounds = 2, file = tmp))
  expect_silent(writeRoundRobin(letters[1:5], rounds = 3, file = tmp))
  united <- expect_silent(setSchedule(united, tmp))
  expect_list(united$schedule, min.len = 1)
})
