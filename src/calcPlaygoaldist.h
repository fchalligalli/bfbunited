#ifndef __calcPlaygoaldist__
#define __calcPlaygoaldist__

std::vector<double> calcPlaygoaldist(
    std::pair<unsigned char, unsigned char> playgoaldist_key
);

#endif
