# Original source: https://gist.github.com/pat-s/7aa990030e0fd25a4eb727592da492d1

# 1.'-e' part: Replaces all level 2 headers and appends a ":" at the end of the line
# 2.'-e' part: Indents all bullet points with a whitespace
# 3.'-e' part: Removes all level 2 headers
# 4.'-e' part: For all level 1 headers, add linebreak and 80 hyphens (not strictly required but clean)
# 5.'-e' part: Remove all level 1 headers
# 6.'-e' part: Remove all backticks

sed -e '/^##/ s/$/:/' -e 's/^*/ */' -e 's/^## *//' -e "/^#/a\\--------------------------------------------------------------------------------" -e 's/^# *//' -e 's/`//g' < NEWS.md > NEWS
