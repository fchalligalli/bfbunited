#ifndef __helpers__
#define __helpers__
using namespace Rcpp;

double fac(unsigned char x);

unsigned ifac(unsigned char x);

double prod(NumericVector x);

double powprod(NumericVector x, IntegerVector y);

#endif
