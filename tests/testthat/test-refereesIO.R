test_that("das Einlesen und Ausgeben der Schiedsrichter funktioniert", {
  referees.file <- system.file("extdata", "example",
                               "Beispielschiedsrichter.ods",
                               package = "bfbUnited", mustWork = TRUE)
  united <- startUnited(parameter.file = NULL, effort.penalties.file = NULL,
                        effort.penalties.name = "Hugo")
  expect_error(setReferees(united, referees.file), "standard")
  united <- startUnited(parameter.file = NULL, effort.penalties.file = NULL)
  united <- expect_silent(setReferees(united, referees.file,
                                      shuffle.names = TRUE))
  expect_data_table(united$referees, types = "character", min.rows = 1)

  referees <- united$referees
  tmp <- tempfile(fileext = ".csv")
  on.exit(unlink(tmp))
  expect_silent(writeReferees(united, tmp))
  united <- expect_silent(setReferees(united, tmp))
  expect_identical(united$referees, referees)
  expect_error(addReferees(united, tmp), "doppelt", ignore.case = TRUE)
})

test_that("Ausgeben und Einlesen von 0 Schiedsrichtern funktioniert", {
  united <- startUnited(parameter.file = NULL, effort.penalties.file = NULL)
  referees <- united$referees
  tmp <- tempfile(fileext = ".csv")
  on.exit(unlink(tmp))
  expect_silent(writeReferees(united, tmp))
  united <- expect_silent(addReferees(united, tmp, shuffle.names = TRUE))
  expect_identical(united$referees, referees)
})
